package smtpManager

import (
	"testing"
	"gopkg.in/gomail.v2"
	"fmt"
	"errors"
	"time"
)

func GetConfig() (*SmtpManagerConfig) {
	return &SmtpManagerConfig{
		QueueSize: 10,
		MaxActiveConnections: 5,
		WatchdogPeriod: 500,
	}
}



func TestSmtpManager_SendMail(t *testing.T) {
	c := GetConfig()

	sc := &TestDaemonSendCloser{
		func(s Sender, msgs ...*gomail.Message) (error){
			return nil
		},
		func() (error){
			return nil
		},
	}

	d := NewTestDialer(
		func() (SendCloser, error){
			return sc, nil
		},
	)

	sm := NewSmtpManager(c, d)

	m := gomail.NewMessage()

	m.SetHeader("From", "noreply@isdschools.org")
	m.SetHeader("To", "jace_ferguson@isdschools.org")
	m.SetHeader("Subject", "Test")
	m.SetBody("text/plain", "Test")

	if err := sm.SendMail(m); err != nil {
		t.Errorf("Unable to send the message. %s", err)
	}
}

/**
This test will fail to dial 5 times, then complete on the 6th
 */
func TestSmtpManager_TestErrorOnDial(t *testing.T) {
	//sm := getManager()
	c := GetConfig()

	dialCount := 0
	sc := &TestDaemonSendCloser{
		func(s Sender, msgs ...*gomail.Message) (error){
			return nil
		},
		func() (error){
			return nil
		},
	}

	d := NewTestDialer(
		func() (SendCloser, error){
			dialCount++
			if dialCount < 5 {
				return nil, errors.New("An error occurred.")
			} else {
				return sc, nil
			}


		},
	)

	sm := NewSmtpManager(c, d)
	//sm.SetLogger(&StdOutLogger{})
	sm.Run()

	if err := sm.SendMail(getMessage()); err != nil {
		t.Errorf("Unable to send the message. %s", err)
	}

}

/**
This will test to make sure a second worker is started once the queue fills up.
 */
func TestSmtpManager_TestWorkerStart(t *testing.T) {
	//sm := getManager()
	c := GetConfig()

	sc := &TestDaemonSendCloser{
		func(s Sender, msgs ...*gomail.Message) (error){
			time.Sleep(5 * time.Second)
			return nil
		},
		func() (error){
			return nil
		},
	}

	d := NewTestDialer(func() (SendCloser, error){	return sc, nil	})

	sm := NewSmtpManager(c, d)
	sm.SetLogger(&StdOutLogger{})
	sm.Run()

	//The queue size is 10
	//Another worker will launch when the queue is more than quarter the capacity, so in this case 3

	go sm.SendMail(getMessage())
	go sm.SendMail(getMessage())
	go sm.SendMail(getMessage())
	go sm.SendMail(getMessage())
	go sm.SendMail(getMessage())

	time.Sleep(500 * time.Millisecond)

	if sm.GetActiveWorkers() != 2 {
		t.Errorf("Extra worker failed to launch.")
	}


}


func getMessage() (*gomail.Message){
	m := gomail.NewMessage()

	m.SetHeader("From", "noreply@isdschools.org")
	m.SetHeader("To", "jace_ferguson@isdschools.org")
	m.SetHeader("Subject", "Test")
	m.SetBody("text/plain", "Test")

	return m
}




type StdOutLogger struct {

}


func(l *StdOutLogger)	Critical(err string) {
	fmt.Printf("Critical:\t %s\n", err)
}
func(l *StdOutLogger)	Debug(err string) {
	fmt.Printf("Debug:\t %s\n", err)
}
func(l *StdOutLogger)	Warning(err string) {
	fmt.Printf("Warning:\t %s\n", err)
}
func(l *StdOutLogger)	Notice(err string) {
	fmt.Printf("Notice:\t %s\n", err)
}
func(l *StdOutLogger)	Info(err string) {
	fmt.Printf("Info:\t %s\n", err)
}
func(l *StdOutLogger)	Fatal(err string) {
	fmt.Printf("Fatal:\t %s\n", err)
}
func(l *StdOutLogger)	Error(err string) {
	fmt.Printf("Error:\t %s\n", err)
}
