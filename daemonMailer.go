package smtpManager

import (
	"io"
	"gopkg.in/gomail.v2"
)

type DaemonMailerBuilder func(host string, port int, username, password string, useAuth bool, ignoreTls bool) DaemonMailer

type DaemonMailer interface {
	Dial() (SendCloser, error)
}

type Sender interface {
	Send(from string, to []string, msg io.WriterTo) error
}

// SendCloser is the interface that groups the Send and Close methods.
type SendCloser interface {
	Sender
	SendMessages(Sender, ...*gomail.Message) (error)
	Close() error
}

type SendFunc func(from string, to []string, msg io.WriterTo) error

