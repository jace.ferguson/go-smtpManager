package smtpManager

import (
	"gopkg.in/gomail.v2"
	"io"
)

type OnDialCallback func() (SendCloser, error)
type OnSendCallback func(s Sender, msgs ...*gomail.Message) (error)
type OnCloseCallback func() (error)

type TestDaemonMailer struct {
	onDial OnDialCallback
}

type TestDaemonSendCloser struct {
	onSend OnSendCallback
	onClose OnCloseCallback
}

func NewTestDialer(onDial OnDialCallback) func(host string, port int, username, password string, useAuth bool, ignoreTls bool) DaemonMailer{
	return func(host string, port int, username, password string, useAuth bool, insecureSkipVerify bool) DaemonMailer {
		dm := &TestDaemonMailer{onDial: onDial}
		return dm
	}
}


func (d *TestDaemonMailer) Dial() (SendCloser, error) {
	return d.onDial()
}

func (sc *TestDaemonSendCloser) SendMessages(s Sender, msgs ...*gomail.Message) error {
	return sc.onSend(s, msgs...)
}

func (sc *TestDaemonSendCloser) Close() error {
	return sc.onClose()
}

func (sc *TestDaemonSendCloser) Send(from string, to []string, msg io.WriterTo) error {
	return nil
}