package smtpManager

import (
	"gopkg.in/gomail.v2"
	"crypto/tls"
)

type GoMailDaemonMailer struct {
	*gomail.Dialer
}

type GoMailSendCloser struct {
	gomail.SendCloser
}


func NewGoMailDialer(host string, port int, username, password string, useAuth bool, insecureSkipVerify bool) DaemonMailer {


	var d *gomail.Dialer
	if useAuth {
		d = gomail.NewDialer(host, port, username, password)
	} else {
		d = &gomail.Dialer{Host: host, Port: port}
	}

	if insecureSkipVerify {

		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}
	return &GoMailDaemonMailer{d}
}


func (d *GoMailDaemonMailer) Dial() (SendCloser, error) {
	newSC, err := d.Dialer.Dial()

	if err != nil {
		return nil, err
	}

	sc := &GoMailSendCloser{newSC}

	return sc, nil
}

func (sc *GoMailSendCloser) SendMessages(s Sender, msgs ...*gomail.Message) error {
	return gomail.Send(s, msgs...)
}

