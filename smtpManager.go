package smtpManager

import (
	"gopkg.in/gomail.v2"
	"time"
	"sync"
	"gitlab.com/jace.ferguson/go-multiFileLog"
	"fmt"
	"math"
)

type SmtpManager struct {
	Config *SmtpManagerConfig
	EmailQueue chan(SmtpSendRequest)
	isInit bool
	activeWorkersWG sync.WaitGroup
	activeConnections int
	logger multiFileLog.Logger
	workerMgmtLock sync.Mutex

	dmBuilder DaemonMailerBuilder

	connCapacity int
}

type SmtpManagerConfig struct {
	QueueSize int `json:"queue_size"`
	MaxActiveConnections int `json:"max_connections"`
	UseAuth bool `json:"use_auth"`
	Host string `json:"host"`
	Port int `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
	WatchdogPeriod int `json:"watchdog_period"`
	InsecureSkipVerify bool `json:"insecure_skip_verify"`
}


type SmtpSendRequest struct {
	Msg *gomail.Message
	StatusChan chan(bool)
	ErrorChan chan(error)
}

func NewSmtpManager(config *SmtpManagerConfig, dm DaemonMailerBuilder) (*SmtpManager) {
	sm := &SmtpManager{}
	sm.logger = multiFileLog.NewNullLogger()
	sm.Config = config
	sm.EmailQueue = make(chan SmtpSendRequest, sm.Config.QueueSize)
	sm.dmBuilder = dm

	if config.MaxActiveConnections > 0 && config.MaxActiveConnections < config.QueueSize{
		sm.connCapacity = int(math.Ceil(float64(config.QueueSize) / float64(config.MaxActiveConnections)))
	} else {
		//A negative or 0 max connections counts means we'll only ever start 1 connection.
		sm.connCapacity = config.QueueSize
	}

	//Start the watchdog.
	return sm

}

func(sm *SmtpManager) Run() {
	sm.logger.Debug("Starting watchdog")
	go sm.Watchdog()
}


func (sm *SmtpManager) Shutdown() {
	sm.logger.Info("Shutting down.")
	close(sm.EmailQueue)
	sm.logger.Info("Waiting for workers to finish.")
	sm.activeWorkersWG.Wait()
	sm.logger.Info("Shutdown complete.")
	return
}

func (sm *SmtpManager) SetLogger(l multiFileLog.Logger) {
	sm.logger = l
}

func (sm *SmtpManager) SendMail(msg *gomail.Message) (error) {
	statusChan := make(chan bool, 1)
	errorChan := make(chan error, 1)

	defer close(statusChan)
	defer close(errorChan)

	req := SmtpSendRequest{
		Msg: msg,
		StatusChan: statusChan,
		ErrorChan: errorChan,
	}

	sm.logger.Debug("Enqueing new message.")
	sm.EmailQueue <- req

	sm.manageWorkers()

	select {
	case <-statusChan:
		return nil

	case err := <-errorChan:
		return err
	}
}


func (sm *SmtpManager) sendMail(ch <-chan(SmtpSendRequest)) {
	sm.activeConnections++
	sm.activeWorkersWG.Add(1)
	defer func() {
		sm.activeConnections--
		sm.activeWorkersWG.Done()
	}()

	d := sm.getDialer()
	var s SendCloser
	var err error

	if s, err = d.Dial(); err != nil {
		sm.logger.Error(fmt.Sprintf("Unable to connect to SMTP server. %s", err))
		return
	}

	for {
		select {
		case r, ok := <-ch:
			if !ok {
				//This will return from the goroutine when the channel closes
				return
			}

			if err := s.SendMessages(s, r.Msg); err != nil {
				r.ErrorChan <- err
				/**
				Until a reset method is added to the gomail library, if we error out we need
				to close the channel because the current transaction cannot be completed.
				 */
				sm.logger.Info("Ending Worker Due to Error on Channel")
				s.Close()
				return

			} else {
				r.StatusChan <- true
			}

			//Check every 30 seconds to see if the email queue is empty.
		case <- time.After(30 * time.Second):
			//If it is, then we can close the connection and return from the goroutine.
			if len(sm.EmailQueue) == 0 {
				s.Close()
				sm.logger.Info("Ending Worker")
				return
			}
		}

	}
}

func (sm *SmtpManager) GetActiveWorkers() (int) {
	return sm.activeConnections
}

func (sm *SmtpManager) getDialer() (DaemonMailer){

	return sm.dmBuilder(sm.Config.Host, sm.Config.Port,
		sm.Config.Username, sm.Config.Password,
		sm.Config.UseAuth, sm.Config.InsecureSkipVerify)
}

/**
There exists a race condition in which the manageWorkers function that gets called upon receiving an email
would show an active connection, however, that active connection may no longer be available by the time the
message is queued. So the watchdog will check ever second to make sure that an active connection is available
if there is mail in the queue.
 */
func (sm *SmtpManager) Watchdog() {
	for {
		select {
		case <- time.After(time.Duration(sm.Config.WatchdogPeriod) * time.Millisecond):
			sm.logger.Debug("Watchdog running")
			sm.manageWorkers()
		}
	}
}

/**
We'll launch new works if the current email queue is over 1/4 full
 */
func (sm *SmtpManager) manageWorkers() {
	sm.workerMgmtLock.Lock()
	defer sm.workerMgmtLock.Unlock()
	defer func() {
		if r := recover(); r != nil {
			sm.logger.Info(fmt.Sprintf("Recovering from panic. Error: %s", r))
		}
	}()

	calcActiveConns := len(sm.EmailQueue) / sm.connCapacity + 1

	if sm.activeConnections == 0 || sm.activeConnections < calcActiveConns {
		sm.logger.Debug("Starting worker.")
		go sm.sendMail(sm.EmailQueue)
	}
}